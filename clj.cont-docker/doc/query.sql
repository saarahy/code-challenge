CREATE EXTENSION IF NOT EXISTS “pgcrypto”;
CREATE EXTENSION IF NOT EXISTS “uuid-ossp”;

-- Table: public._role

-- DROP TABLE public._role;

CREATE TABLE public._role
(
    id integer NOT NULL DEFAULT nextval('_role_id_seq'::regclass),
    role_name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT _role_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public._role
    OWNER to postgres;


-- Table: public._status

-- DROP TABLE public._status;

CREATE TABLE public._status
(
    id integer NOT NULL DEFAULT nextval('_status_id_seq'::regclass),
    status_name text COLLATE pg_catalog."default" NOT NULL,
    status_type text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT _status_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public._status
    OWNER to postgres;

-- Table: public._user

-- DROP TABLE public._user;

CREATE TABLE public._user
(
    id integer NOT NULL DEFAULT nextval('_user_id_seq'::regclass),
    uuid uuid DEFAULT uuid_generate_v4(),
    username text COLLATE pg_catalog."default" NOT NULL,
    passw text COLLATE pg_catalog."default" NOT NULL,
    created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    role_ integer NOT NULL,
    status_ integer NOT NULL,
    CONSTRAINT _user_pkey PRIMARY KEY (id),
    CONSTRAINT fk_user_role FOREIGN KEY (role_)
        REFERENCES public._role (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_user_status FOREIGN KEY (status_)
        REFERENCES public._status (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public._user
    OWNER to postgres;



-- Table: public._record

-- DROP TABLE public._record;

CREATE TABLE public._record
(
    id integer NOT NULL DEFAULT nextval('_record_id_seq'::regclass),
    uuid uuid NOT NULL DEFAULT uuid_generate_v4 (),,
    service_id integer NOT NULL,
    user_id integer NOT NULL,
    user_balance numeric NOT NULL,
    service_response text COLLATE pg_catalog."default" NOT NULL,
    date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT _record_pkey PRIMARY KEY (id),
    CONSTRAINT fk_record_serv FOREIGN KEY (service_id)
        REFERENCES public._service (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_record_user FOREIGN KEY (user_id)
        REFERENCES public._user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

ALTER TABLE public._record
    OWNER to postgres;


-- Table: public._service

-- DROP TABLE public._service;

CREATE TABLE public._service
(
    id integer NOT NULL DEFAULT nextval('_service_id_seq'::regclass),
    uuid uuid NOT NULL DEFAULT uuid_generate_v4 (),,
    type text COLLATE pg_catalog."default" NOT NULL,
    cost numeric NOT NULL,
    status_ integer NOT NULL,
    CONSTRAINT _service_pkey PRIMARY KEY (id),
    CONSTRAINT fk_service_status FOREIGN KEY (status_)
        REFERENCES public._status (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public._service
    OWNER to postgres;


INSERT INTO public._status(
     status_name, status_type)
    VALUES ('active', 'admin');

INSERT INTO public._status(
     status_name, status_type)
    VALUES ('trial, 'admin')

INSERT INTO public._status(
     status_name, status_type)
    VALUES ('inactive', 'admin')


INSERT INTO public._status(
     status_name, status_type)
    VALUES ('inactive', 'service')

INSERT INTO public._status(
     status_name, status_type)
    VALUES ('active', 'service')

INSERT INTO public._status(
     status_name, status_type)
    VALUES ('beta', 'service');

INSERT INTO public._role(
     role_name)
    VALUES ('admin');

INSERT INTO public._role(
     role_name)
    VALUES ('user');

Select * from _status;
INSERT INTO public._service(
     type, cost, status_)
    VALUES ('addition', 100.0, 5);
Select * from _service;
INSERT INTO public._record(
    service_id, user_id, user_balance, service_response)
    VALUES ( 2, 1, 400.9, 'none');
Select * from _record;

