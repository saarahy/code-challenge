(defproject myapp "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 ; Compojure - A basic routing library
                 [compojure "1.6.1"]
                 ; Our Http library for client/server
                 [http-kit "2.3.0"]
                 ; Ring defaults - for query params etc
                 [ring/ring-defaults "0.3.2"]
                 ;A data-driven router for Clojure/Script
                 [metosin/reitit "0.5.12"]
                 ;Dependencies to use postgresql
                 [org.clojure/java.jdbc "0.7.3"]
                 [org.postgresql/postgresql "42.1.4"] 
                 [clojure.jdbc/clojure.jdbc-c3p0 "0.3.3"]
                 ;hugsql
                 [com.layerware/hugsql "0.4.8"]
                 ]
  :plugins [[lein-ring "0.12.5"]]
  :main ^:skip-aot myapp.core
  :ring {:handler myapp.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}})
