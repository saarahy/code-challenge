(ns myapp.db.calc
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "myapp/db/sql/calc.sql")