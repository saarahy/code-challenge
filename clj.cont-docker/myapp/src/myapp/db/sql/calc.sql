-- :name create-user-table
-- :command :execute
-- :result :raw
create table _user (
  id serial primary key,
  uuid uuid DEFAULT uuid_generate_v4 (),
  username text not null,
  passw text not null,
  role_ text not null,
  status_ text not null,
  created_at timestamp not null default current_timestamp
);

/*** Inserts  ***/

-- :name insert-user :! :n 
-- :doc insert a new user
insert into _user (username, passw, role_, status_)
values (:username, PGP_SYM_ENCRYPT(:passw,'AES_KEY'), :role_, :status_)
returning id;

/*** Selections  ***/

-- :name get-user-by-id :? :1 
-- :doc Getting all user's data from id
select * from _user where id = :id;


-- :name get-user-t :? :* 
-- :doc Getting a list of all users
select * from _user;

--:name get-role :? :*
-- :doc Getting a list of all roles
select * from _role;

--:name get-services :? :*
-- :doc Getting a list of all services
select * from _service;

--:name get-user-balance-list :? :*
-- :doc Getting a list of the user's balance
select u.username, u.uuid, r.user_balance, r.date
from _user u inner join _record r on u.id = r.user_id
where u.id= :id

--:name get-user-report :? :*
-- :doc Getting a list of all the movements of the user 
select u.username, r.user_balance, s.type, s.cost, r.service_response, r.date
from _user u inner join _record r on u.id = r.user_id
inner join _service s on r.service_id=s.id
where u.id=:id

/*DELETES*/
--:name delete-user-by-id :! :1  
DELETE FROM _user WHERE id = :id;

/* UPDATES */
--:name update-user-by-id :! :1
UPDATE _user
SET username = :username, passw= :passw, role_= :role_, status_ = :status_
WHERE id= :id;