(ns myapp.handler
  (:require ;[compojure.core :refer :all]
            ;[compojure.route :as route]
            ;[ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [myapp.db :as db]		
            [myapp.db.calc :as db-calc]))

(defn get-users [_] {
  :status 200
  :body (db-calc/get-user-t db/config)                       
})

(defn get-user-by-id 
  [{:keys [parameters]}]
  (let [id (:path parameters)]
    {:status 201
     :body (db-calc/get-user-by-id db/config id)}))

(defn create-user
  [{:keys [parameters]}]
  (let [data (:body parameters)
        created-id (db-calc/insert-user db/config data)]
    {:status 201
     :body (db-calc/get-user-by-id db/config created-id)}))

(defn update-user
  [{:keys [parameters]}]
  (let [id (get-in parameters [:path :id])
        body (:body parameters)
        data (assoc body :id id)
        updated-count (db-calc/update-user-by-id db/config data)
        ]
    (if (= 1 updated-count)
      {:status 200
       :body {:updates true
              :user (db-calc/get-user-by-id db/config {:id id})}}
      {:status 404
       :body {:error "Unable to update user"}})))

(defn delete-user
  [{:keys [parameters]}]
  (let [id (:path parameters)
        before-deleted (db-calc/get-user-by-id db/config id)
        deleted-count (db-calc/delete-user-by-id db/config id)]
    (if (= 1 deleted-count)
      {:status 200
       :body {:deleted true
              :contact before-deleted}}
      {:status 404
       :body {:error "Unable to delete user"}})))
