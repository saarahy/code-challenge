(ns myapp.core
	(:require [org.httpkit.server :refer [run-server]]
	[muuntaja.core :as m]
	[reitit.ring :as ring]
  [reitit.ring.middleware.parameters :refer [parameters-middleware]]
	[reitit.ring.middleware.exception :refer [exception-middleware]]
	[reitit.ring.middleware.muuntaja :refer [format-negotiate-middleware
                                                     format-request-middleware
                                                     format-response-middleware]]
 	[reitit.ring.coercion :refer [coerce-exceptions-middleware
                                coerce-request-middleware
                                coerce-response-middleware]]
  [reitit.coercion.schema]
  [schema.core :as s]
  ;[myapp.db :as db]		
  ;[myapp.db.calc :as db-calc]						 
  [myapp.routes :refer [ping-routes user-routes]]
))

(defonce server (atom nil))

(def app
  (ring/ring-handler
    (ring/router
      [["/api"
        ;["/ping" {:get (fn [req] {:status 200
        ;                          :body {:ping "pong"}})}]
        ;["/contacts" {:get (fn [req] {:status 200
        ;                              :body (db-calc/get-user-t db/config)})}]
        ping-routes
        user-routes
        ]]
      {:data {:coercion reitit.coercion.schema/coercion
              :muuntaja m/instance
              :middleware [
				  			;[wrap-cors
                            ;:access-control-allow-origin [#"http://localhost:4200"]
                            ;:access-control-allow-methods [:get :post :put :delete]]
                           parameters-middleware
                           format-negotiate-middleware
                           format-response-middleware
                           exception-middleware
                           format-request-middleware
                           coerce-exceptions-middleware
                           coerce-request-middleware
                           coerce-response-middleware
						   ]}})
    (ring/routes
      (ring/redirect-trailing-slash-handler)
      (ring/create-default-handler
        {:not-found (constantly {:status 404 :body "Route not found"})}))))

(defn stop-server []
	(when-not (nil? @server)
		(@server :timeout 100)
		(reset! server nil)))


(defn -main []
	(println "Server started")
	(run-server app {:port 4000}))

(defn restart-server []
	(stop-server)
	(-main))
