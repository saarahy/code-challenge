(ns myapp.db
  ;(:require [hugsql.core :as hugsql])
  (:require [jdbc.pool.c3p0 :as pool]))

(def config 
  (pool/make-datasource-spec 
  {:classname "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname "//172.21.0.3:5432/clj_calc"
   :user "postgres"
   :password "postgres"}))

;(hugsql/def-db-fns "myapp/db/calc.sql")
