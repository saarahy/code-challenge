(ns myapp.routes
  (:require [schema.core :as s]
   [myapp.handler :refer [get-users
                                   get-user-by-id
                                   create-user
                                   update-user
                                   delete-user]]))

(def ping-routes ["/ping" {:get (fn [req]
                 {:status 200
                  :body {:hello "world"}})}])

; (def user-routes ["/users" {:get get-users}])

(def user-routes ["/users" 
                  ["/" {:get get-users
                        :post {:parameters {:body {:username s/Str
                                                   ;:last-name s/Str
                                                   ;:email s/Str
                                                   }}
                               :handler create-user}}]
                  ["/:id" {:parameters {:path {:id s/Int}}
                           :get get-user-by-id
                           :put {:parameters {:body {:username s/Str
                                                     ;:last-name s/Str
                                                     ;:email s/Str
                                                     }}
                                 :handler update-user}
                           :delete delete-user}]])
