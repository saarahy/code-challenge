# code-challenge

This is a code challenge for the LoanPro Company. 

# Introduction

This is an implement of a Web platform to provide a simple calculator functionality (addition subtraction,multiplication,division,squareroot,free form and a random string generation where each functionality will have a separate cost per request. This web platform will contain two type of users, and each of them will have some constraints.

## Achievements--

Since I read the project I visualized it and I liked it. Which gave me a guideline to venture into an area that I had hardly explored, clojure. I had use the Clojure language to create algorithms, but not to generate the logic of a web page, however I decided to venture into that area and although the project is not finished, it was a success (at least for me).

I learned a lot from the language and I consider it too smooth for this type of technology, both the architectural side and the tools it offer you are very versatile and easy to understand. I honestly had some problems with some librearies, which made me fall behind in the project.

Currently I present the architecture of the backend, the construction of the database and part of the controllers, leaving out the frontend part.

## Instructions
1. First of all we will setup the database. Run the docker compose at myapp file:
    ```
    $ docker-compose up -d
    ```
1. Check the IP Address for the PgAdmin
    ```
    $ sudo docker inspect <container-id> | grep IPAddress
    ```
1. Maybe you will have to change de IP Address at the [db.clj](https://gitlab.com/saarahy/code-challenge/-/blob/master/clj.cont-docker/myapp/src/myapp/db.clj) file
```
:subname "//172.21.0.3:5432/clj_calc"
```
1. Run the  [query.sql](https://gitlab.com/saarahy/code-challenge/-/blob/master/clj.cont-docker/doc/query.sql) at the PgAdmin 
1. Run the docker-JDK Dockerfile
1. Run clj.cont-docker Dockerfile
   1. Inside the container run:
   ```
    $ lein run
    ```